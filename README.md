## Gerenciador de Tarefas

Seguir os seguintes processos para utilização deste projeto.

1. Faça o clone deste projeto.
2. Acesse via terminal o diretorio onde foi clonado
3. Certifique de ter instalado o Java 11
4. Rode o comando `mvn clean install` 
5. Caso queira rodar o .jar execute o comando `java -jar target/gerenciador-tarefa-0.0.1-SNAPSHOT.jar` 
6. Caso contrario você poderá rodar direto na sua IDE ou pelo comando: `mvn spring-boot:run`
7. Para testar se deu certo acessar a API pela URL: `http://localhost:8080/tarefas`. Por padrão sempre é criado duas tarefas de teste.

---

## EndPoints da API

- Utilizar um API Client de sua preferencia para fazer as requisições. Eu utilizo o `https://insomnia.rest`

- Buscar todas as tarefas realizadas
```bash
GET
`https://localhost:8080/tarefas`
```

- Buscar tarefas por Id
```bash
GET
`https://localhost:8080/tarefa/2`
```

- Criar uma nova tarefa
```bash
POST
`https://localhost:8080/tarefa`

Exemplo body:
{
	"descricao": "Descrição da tarefa",
	"nomeCliente":"Antonio da Silva",
	"telefone":"19997596504",
	"dataNascimento":"25/01/1950"
}
```
---

## Acessando o BD H2

Utilize esta etapa caso queira acessar o banco de dados H2.

1. Com a aplicação em execução acesse: `http://localhost:8080/h2-ui`
2. No campo Driver Class informe: org.h2.Driver
3. No campo JDBC URL informe: jdbc:h2:mem:tarefadb
4. No campo User e Pass deixar o padrão
5. Clique em test connection e depois em connect
6. Com o acesso realizado certifique que foi criado a table: TAREFA
7. Para teste você poderá rodar: `SELECT * FROM TAREFA`. Por padrão caso não tenha inserido nenhuma tarefa o retorno será de 2 tarefas.
