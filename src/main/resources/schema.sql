DROP TABLE IF EXISTS tarefa;

CREATE TABLE tarefa (
	id INT AUTO_INCREMENT PRIMARY KEY,
	descricao VARCHAR(255) NOT NULL,
	nome_cliente VARCHAR(255),
	data_nascimento DATE,
	telefone VARCHAR(20)
);
