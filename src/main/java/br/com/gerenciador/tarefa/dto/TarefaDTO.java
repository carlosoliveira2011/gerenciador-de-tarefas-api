package br.com.gerenciador.tarefa.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import br.com.gerenciador.tarefa.entity.Tarefa;
import br.com.gerenciador.tarefa.util.DateUtil;

public class TarefaDTO implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private static final String PATTERN_DATE = "dd/MM/yyyy";
	
	@NotNull(message="Campo id não pode ser vazio.")
	private long id;
	
	@NotNull(message="Campo descricao não pode ser vazio.")
	private String descricao;
	
	@NotNull(message="Campo nomeCliente não pode ser vazio.")
	private String nomeCliente;
	
	@NotNull(message="Campo dataNascimento não pode ser vazio.")
	private String dataNascimento;
	
	@NotNull(message="Campo telefone não pode ser vazio.")
	private String telefone;
	
	public Tarefa transformaDTOToEntity (TarefaDTO dto) throws Exception {
		Tarefa tarefa = new Tarefa();
		tarefa.setDescricao(dto.getDescricao());
		tarefa.setNomeCliente(dto.getNomeCliente());
		tarefa.setDataNascimento(DateUtil
				.stringDateParaDatePattern(dto.getDataNascimento(), PATTERN_DATE));
		tarefa.setTelefone(dto.getTelefone());
		return tarefa;
	}
	
	public TarefaDTO transformaEntityToDTO (Tarefa tarefa) {
		TarefaDTO tarefaDTO = new TarefaDTO();
		tarefaDTO.setId(tarefa.getId());
		tarefaDTO.setDescricao(tarefa.getDescricao());
		tarefaDTO.setNomeCliente(tarefa.getNomeCliente());
		tarefaDTO.setDataNascimento(DateUtil
				.localDateParaDatePattern(tarefa.getDataNascimento(), PATTERN_DATE));
		tarefaDTO.setTelefone(tarefa.getTelefone());
		return tarefaDTO;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getNomeCliente() {
		return nomeCliente;
	}
	public void setNomeCliente(String nomeCliente) {
		this.nomeCliente = nomeCliente;
	}
	public String getDataNascimento() {
		return dataNascimento;
	}
	public void setDataNascimento(String dataNascimento) {
		this.dataNascimento = dataNascimento;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
