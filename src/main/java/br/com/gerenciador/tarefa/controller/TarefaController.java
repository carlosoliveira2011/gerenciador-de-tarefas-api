package br.com.gerenciador.tarefa.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.gerenciador.tarefa.dto.TarefaDTO;
import br.com.gerenciador.tarefa.service.TarefaService;

@RestController
public class TarefaController {
	
	@Autowired
	private TarefaService tarefaService;
	
	@GetMapping("tarefa/{id}")
	public ResponseEntity<TarefaDTO> getTarefaById(@PathVariable("id") long id) {
		TarefaDTO tarefaDTO = tarefaService.getTarefaById(id);
		return new ResponseEntity<TarefaDTO>(tarefaDTO, HttpStatus.OK);
	}
	
	@GetMapping("tarefas")
	public ResponseEntity<List<TarefaDTO>> getAllTarefas() {
		List<TarefaDTO> tarefasDTO = tarefaService.getAllTarefas();
		return new ResponseEntity<List<TarefaDTO>>(tarefasDTO, HttpStatus.OK);
	}
	
	@PostMapping("tarefa")
	public ResponseEntity<Void> addTarefa(@RequestBody @Valid TarefaDTO tarefaDTO) throws Exception {
		tarefaService.addTarefa(tarefaDTO);
		return new ResponseEntity<Void>(HttpStatus.CREATED);
	}

}
