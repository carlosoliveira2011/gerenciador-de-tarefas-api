package br.com.gerenciador.tarefa.service;

import java.util.List;

import br.com.gerenciador.tarefa.dto.TarefaDTO;

public interface TarefaService {
	
	List<TarefaDTO> getAllTarefas();
	
	TarefaDTO getTarefaById(long id);
	
	boolean addTarefa(TarefaDTO tarefaDTO) throws Exception;
	
}
