package br.com.gerenciador.tarefa.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.gerenciador.tarefa.dto.TarefaDTO;
import br.com.gerenciador.tarefa.repository.TarefaRepository;

@Service
public class TarefaServiceImpl implements TarefaService {
	
	@Autowired
	private TarefaRepository tarefaRepository;

	@Override
	public List<TarefaDTO> getAllTarefas() {
		List<TarefaDTO> tarefasDTO = new ArrayList<TarefaDTO>();
		
		tarefaRepository.findAll().forEach(tarefa -> tarefasDTO
				.add(new TarefaDTO().transformaEntityToDTO(tarefa))
		);
		return tarefasDTO;
	}

	@Override
	public TarefaDTO getTarefaById(long id) {
		return new TarefaDTO().transformaEntityToDTO(tarefaRepository.findById(id).get());
	}

	@Override
	public boolean addTarefa(TarefaDTO tarefaDTO) throws Exception {
		tarefaRepository.save(tarefaDTO.transformaDTOToEntity(tarefaDTO));
		return true;
	}
	
}
