package br.com.gerenciador.tarefa.util;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class DateUtil {
	
	public static LocalDate stringDateParaDatePattern(String stringData, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		try {
			return LocalDate.parse(stringData, formatter);
		} catch (Exception ex) {
            throw new DateTimeParseException("Erro na conversão da data. Formato deve ser dd/MM/yyyy", ex.getMessage(), 0);
		}
	}
	
	public static String localDateParaDatePattern(LocalDate localDate, String pattern) {
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(pattern);
		try {
			return localDate.format(formatter);
		} catch (Exception ex) {
            throw new DateTimeParseException("Erro interno na formatação da data.", "", 0);
		}
	}
}
