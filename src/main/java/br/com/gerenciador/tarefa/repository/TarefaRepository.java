package br.com.gerenciador.tarefa.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import br.com.gerenciador.tarefa.entity.Tarefa;

public interface TarefaRepository extends JpaRepository<Tarefa, Long>{

}
