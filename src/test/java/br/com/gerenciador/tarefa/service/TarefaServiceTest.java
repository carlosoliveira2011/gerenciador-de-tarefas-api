package br.com.gerenciador.tarefa.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import br.com.gerenciador.tarefa.dto.TarefaDTO;
import br.com.gerenciador.tarefa.entity.Tarefa;
import br.com.gerenciador.tarefa.repository.TarefaRepository;
import br.com.gerenciador.tarefa.util.DateUtil;

@ExtendWith(MockitoExtension.class)
public class TarefaServiceTest {
	
	@Mock
	TarefaRepository tarefaRepository;
	
	@InjectMocks
	TarefaServiceImpl tarefaServiceImpl;
	
	@Test
	void testGetAllTarefas() {
		List<Tarefa> tarefas = buildTarefas();
		when(tarefaRepository.findAll()).thenReturn(tarefas);

		List<TarefaDTO> result = tarefaServiceImpl.getAllTarefas();
		assertThat(result.size()).isEqualTo(1);
	}
	
	@Test
	void testGetTarefaById() {
		Tarefa tarefa = buildTarefa();
		long idTarefa = 1L;
		when(tarefaRepository.findById(idTarefa)).thenReturn(Optional.of(tarefa));
		
		TarefaDTO result = tarefaServiceImpl.getTarefaById(1L);
		assertThat(result.getId()).isEqualTo(tarefa.getId());
		assertThat(result.getDescricao()).isEqualTo(tarefa.getDescricao());
		assertThat(result.getNomeCliente()).isEqualTo(tarefa.getNomeCliente());
		assertThat(result.getTelefone()).isEqualTo(tarefa.getTelefone());
		assertThat(DateUtil.stringDateParaDatePattern(result.getDataNascimento(), "dd/MM/uuuu")).isEqualTo(tarefa.getDataNascimento());
	}
	
	@Test
	void testAddTarefa() throws Exception {
		TarefaDTO tarefaDTO = buildTarefaDTO();
		
		when(tarefaRepository.save(Mockito.any(Tarefa.class)))
			.thenAnswer(i -> i.getArguments()[0]);

		Boolean result = tarefaServiceImpl.addTarefa(tarefaDTO);

		assertThat(result).isEqualTo(Boolean.TRUE);
	}
	
	//TODO nao fiz a cobertura de todos os cenarios.
	
	private List<Tarefa> buildTarefas() {
		Tarefa tarefa = new Tarefa();
		tarefa.setId(1L);
		tarefa.setDescricao("tarefa de teste");
		tarefa.setNomeCliente("Nome de teste");
		tarefa.setId(1L);
		tarefa.setTelefone("19999999905");
		tarefa.setDataNascimento(LocalDate.of(1988, 03, 30));
		return Collections.singletonList(tarefa);
	}
	
	private TarefaDTO buildTarefaDTO() {
		TarefaDTO tarefaDTO = new TarefaDTO();
		tarefaDTO.setId(1L);
		tarefaDTO.setDescricao("tarefa de teste");
		tarefaDTO.setNomeCliente("Nome de teste");
		tarefaDTO.setId(1L);
		tarefaDTO.setTelefone("19999999905");
		tarefaDTO.setDataNascimento("30/03/1988");
		return tarefaDTO;
	}
	
	private Tarefa buildTarefa() {
		Tarefa tarefa = new Tarefa();
		tarefa.setId(1L);
		tarefa.setDescricao("tarefa de teste");
		tarefa.setNomeCliente("nome do cliente");
		tarefa.setDataNascimento(LocalDate.of(1988, 03, 30));
		tarefa.setTelefone("19999999905");
		return tarefa;
	}

}
